# Tracing

`libtracing.so` is compiled with `-std=c++11 -static-libstdc++ -static-libgcc -fno-exceptions -fno-rtti -fPIC` on Ubuntu 16.04.

Trace files are printed to `stderr`, redirect with `2> trace.json`, view in `about:tracing` browser tab.